;(function (factory) {
    'use strict'
    new factory(window.jQuery, window.fafcms)
})(function ($, fafcms) {
    'use strict'

    var addActionButtonValue = function($actionButton, formSelector) {
        if (typeof $actionButton.data('submit-name') !== 'undefined') {
            var name = $actionButton.data('submit-name')
        }
        else if (typeof $actionButton.attr('name') !== 'undefined') {
            var name = $actionButton.attr('name')
        }

        if (typeof $actionButton.data('submit-value') !== 'undefined') {
            var value = $actionButton.data('submit-value')
        }
        else if (typeof $actionButton.attr('value') !== 'undefined') {
            var value = $actionButton.attr('value')
        }

        if (typeof name !== 'undefined' && typeof value !== 'undefined') {
            $(formSelector).append('<input type="hidden" name="' + name + '" value="' + value + '" />')
        }
    }

    fafcms.prototype.events['init'].push(function () {
        $('body').on('click', 'button[type="submit"][form]', function(e) {
            e.preventDefault()

            var $actionButton = $(this)
            var formSelector = $actionButton.attr('form')

            if (typeof formSelector === 'undefined') {
                formSelector = $actionButton.data('form-selector')
            } else {
                formSelector = '#' + formSelector
            }

            var title = $actionButton.data('confirm-modal')

            if (typeof title === 'undefined') {
                title = $actionButton.data('confirm')
            }

            if (typeof title !== 'undefined') {
                var cancel = $actionButton.data('confirm-modal-cancel')
                var confirm = $actionButton.data('confirm-modal-confirm')
                var message = $actionButton.data('message')

                fafcms.prototype.openConfirmModal({
                    title: title,
                    cancel: cancel,
                    confirm: confirm,
                    message: message,
                    actionConfirmed: function () {
                        addActionButtonValue($actionButton, formSelector)
                        $(formSelector).submit()
                    }
                })
            }
            else {
                addActionButtonValue($actionButton, formSelector)
                $(formSelector).submit()
            }
        })
    })

    return fafcms
})
