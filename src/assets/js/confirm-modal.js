;(function (factory) {
    'use strict'
    new factory(window.jQuery, window.fafcms)
})(function ($, fafcms) {
    'use strict'

    fafcms.prototype.openConfirmModal = function(options) {
        var confirmed = false

        try {
            $('#confirm-modal').modal('close')
        } catch(ex) {}

        $('#confirm-modal').modal({
            opacity: .7,
            onCloseEnd: function() {
                if (!confirmed && typeof options.actionCanceled !== 'undefined') {
                    options.actionCanceled()
                }
            }
        })

        $('#confirm-modal').html('<div class="modal-content valign-wrapper flex-center">' + fafcms.prototype.loader + '</div>')
        $('#confirm-modal').modal('open')

        if (typeof options.title === 'undefined') {
            options.title = yii.t('app', 'Do you really want to perform this action?')
        }

        if (typeof options.cancel === 'undefined') {
            options.cancel = yii.t('app', 'Cancel')
        }

        if (typeof options.confirm === 'undefined') {
            options.confirm = yii.t('app', 'Yes')
        }

        if (typeof options.message === 'undefined') {
            options.message = yii.t('app', 'Please confirm the action by clicking Yes.')
        }

        $('#confirm-modal').html(
            '<button class="btn waves-effect waves-light modal-close modal-header-close"><i class="mdi mdi-close"></i></button>' +
            '<div class="modal-content">' +
                '<header class="modal-header">' +
                    '<h4>' + options.title + '</h4>' +
                '</header>' +
                '<div class="row">' +
                    '<div class="col s12">' +
                        options.message +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="modal-footer">' +
                '<div class="row">' +
                    '<div class="col s12 m6">' +
                        '<button id="confirm-modal-cancel" class="btn full-width waves-effect waves-light modal-close secondary"><i class="mdi mdi-close left"></i>' + options.cancel + '</divbutton>' +
                    '</div>'+
                    '<div class="col s12 m6">' +
                        '<button id="confirm-modal-confirm" class="btn full-width waves-effect waves-light primary"><i class="mdi mdi-check-outline left"></i>' + options.confirm + '</button>' +
                    '</div>'+
                '</div>'+
            '</div>'
        )

        $('#confirm-modal-confirm').click(function () {
            confirmed = true
            options.actionConfirmed()
            $('#confirm-modal').modal('close')
        })
    }

    fafcms.prototype.events['init'].push(function () {
        if ($('#confirm-modal').length === 0) {
            $('body').append('<div id="confirm-modal" class="modal"></div>')
        }

        $('body').on('click', '[data-confirm-modal]:not(button[type="submit"][form])', function(e) {
            e.preventDefault()

            var $confirmButton = $(this)
            var title = $confirmButton.data('confirm-modal')
            var cancel = $confirmButton.data('confirm-modal-cancel')
            var confirm = $confirmButton.data('confirm-modal-confirm')
            var message = $confirmButton.data('confirm-modal-message')

            fafcms.prototype.openConfirmModal({
                title: title,
                cancel: cancel,
                confirm: confirm,
                message: message,
                actionConfirmed: function () {
                    window.location.href = $confirmButton.attr('href')
                }
            })
        })
    })

    return fafcms
})
