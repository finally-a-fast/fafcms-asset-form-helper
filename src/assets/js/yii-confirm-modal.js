;(function ($) {
    'use strict'
    window.yii.confirm = function (title, ok, cancel) {
        if ($(this).not('button[type="submit"][form]').length > 0) {
            fafcms.prototype.openConfirmModal({
                title: title,
                message: $(this).data('message'),
                actionCanceled: cancel,
                actionConfirmed: ok
            })
        }
    }
})(window.jQuery)
