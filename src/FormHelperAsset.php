<?php

namespace fafcms\assets\formhelper;

use yii\web\AssetBundle;

class FormHelperAsset extends AssetBundle
{
    public $sourcePath = __DIR__.'/assets';

    public $js = [
        'js/confirm-modal.js',
        'js/yii-confirm-modal.js',
        'js/form-helper.js',
    ];

    public $depends = [
        'fafcms\assets\init\InitAsset',
        'yii\web\YiiAsset',
    ];
}
